package adios.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import adios.model.Filter;
import adios.model.Recipe;
import adios.model.User;
import adios.service.RecipeService;

@Controller
public class SearchController {

	protected final Log Log = LogFactory.getLog(getClass());

	@Autowired
	private RecipeService recipeService;

	@RequestMapping(value = "/campo/busqueda/nombre.html", method = RequestMethod.POST)
	public ModelAndView searchRecipNameByField(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("s");
		Log.info("campo/busqueda/nombre :::<<" + id);

		ModelAndView view;
		view = new ModelAndView("html/mainTemplate");
		view.addObject("recipeList", recipeService.getRecipeForName(id));
		return view;
	}

	@RequestMapping(value = "/campo/busqueda/tag.html", method = RequestMethod.POST)
	public ModelAndView searchTagByField(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("s");
		Log.info("/campo/busqueda/tag :::<<" + id);

		ModelAndView view;
		view = new ModelAndView("html/mainTemplate");
		Filter f = new Filter();
		f.setType("tag");
		f.setId(id);
		ArrayList<Filter> filters =  new ArrayList<Filter>();
		filters.add(f);
		ArrayList<Recipe> rList = recipeService.getRecipeForFilter(filters);
		view.addObject("recipeList", rList);
		return view;
	}

	@RequestMapping(value = "/busqueda/tag/{id}.html", method = RequestMethod.GET)
	public ModelAndView searchTagByLink(@PathVariable String id,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Log.info("/busqueda/tag/{id} :::" + id);

		ModelAndView view;
		view = new ModelAndView("html/mainTemplate");
		Filter f = new Filter();
		f.setType("tag");
		f.setId(id);
		ArrayList<Filter> filters =  new ArrayList<Filter>();
		filters.add(f);
		ArrayList<Recipe> rList = recipeService.getRecipeForFilter(filters);
		view.addObject("recipeList", rList);
		return view;
	}

	@RequestMapping(value = "/campo/busqueda/ingrediente.html", method = RequestMethod.POST)
	public ModelAndView searchIngByField(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("s");

		Log.info("/busqueda/ingrediente/{id} :::<<" + id);

		ModelAndView view;
		view = new ModelAndView("html/mainTemplate");
		ArrayList<Recipe> rList = recipeService.getRecipeForIngredientName(id);
		view.addObject("recipeList", rList);
		return view;
	}

	@RequestMapping(value = "/busqueda/ingrediente/{name}.html", method = RequestMethod.GET)
	public ModelAndView searchIngByLink(@PathVariable String name,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Log.info("/busqueda/ingrediente/{name} :::" + name);
		ModelAndView view;
		view = new ModelAndView("html/mainTemplate");
		ArrayList<Recipe> rList = recipeService
				.getRecipeForIngredientName(name);
		view.addObject("recipeList", rList);
		return view;
	}

	@RequestMapping(value = "/filtro.html", method = RequestMethod.POST)
	public ModelAndView searchByFilter(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Log.info("/filtro.html");
		ModelAndView view;
		view = new ModelAndView("html/components/recipeListDisplay");
		String jsonFilters = request.getParameter("filters");
		ArrayList<Recipe> rList = new ArrayList<Recipe>();
		try {
			Gson gson = new Gson();
			ArrayList filterList = gson.fromJson(jsonFilters, ArrayList.class);
			ArrayList<Filter> filters=new ArrayList<Filter>();
			for(int i=0; i<filterList.size();i++){
				filters.add(gson
						.fromJson(filterList.get(i).toString(), Filter.class));

			}
			rList= recipeService.getRecipeForFilter(filters);
			view.addObject("recipeList", rList);
		} catch (Exception e) {
			rList= recipeService.getLastRecipes(10, 0);
			view.addObject("recipeList", rList);			
			Log.error("error filtering::: " + e.getMessage());
			return view;
		}

		
		
		
		return view;

	}

}
