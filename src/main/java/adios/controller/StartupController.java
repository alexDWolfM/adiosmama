package adios.controller;

import java.io.IOException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import adios.model.UserRole;
import adios.service.StartUpService;

@Controller
public class StartupController {

	private static final Log Log = LogFactory.getLog(UserRole.class);

	@Autowired
	StartUpService startUpService;
	
    @RequestMapping(value="/initData.html", method = RequestMethod.GET)
    public ModelAndView signUpGetHandleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Log.info("--------------------------------------------------------------");
    	Log.info("fill up START!!!!!!");

    	startUpService.start();

    	Log.info(" returning to welcome page html/mainTemplate.jsp");
    	Log.info("fill up END!!!!!!");
        Log.info("--------------------------------------------------------------");

    	ModelAndView view = new ModelAndView("html/mainTemplate");
        return view;
	}
    
    
    @RequestMapping(value="/dropData.html", method = RequestMethod.GET)
    public ModelAndView dropData(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Log.info("--------------------------------------------------------------");
    	Log.info("fill up START!!!!!!");

//    	EntityManagerFactory emf =
//    	        Persistence.createEntityManagerFactory("myDbFile.odb;drop");
        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("/db/adiosMama.odb;drop");


    	Log.info(" returning to welcome page html/mainTemplate.jsp");
    	Log.info("fill up END!!!!!!");
        Log.info("--------------------------------------------------------------");

    	ModelAndView view = new ModelAndView("html/mainTemplate");
        return view;
	}
}
