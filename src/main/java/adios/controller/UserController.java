package adios.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import adios.model.Recipe;
import adios.model.User;
import adios.service.RecipeService;
import adios.service.UserService;


//import pfc.adiosMama.model.obj.adiosMama.Users;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;

	@Autowired
	RecipeService recipeService;

	 protected final Log Log = LogFactory.getLog(getClass());
	 protected static final String loginUser = "LOGGEDIN_USER";
	 
    @RequestMapping(value="/signUp.html", method = RequestMethod.GET)
    public ModelAndView signUpGetHandleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Log.info("signUpHandleRequest");
//        Users u =test.getUser();
//        Log.info("-------------------------------"+u.getId());
    	ModelAndView view = new ModelAndView("html/signUp");
    	
        return view;
	}

    @RequestMapping(value="/signUp.html", method = RequestMethod.POST)
    public ModelAndView signUpPostHandleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Log.info("signUpHandleRequest");

    	ModelAndView view = new ModelAndView("html/loginPage");
    	
    	User user= new User();
    	user.setMail(request.getParameter("mail"));
    	user.setPassword(request.getParameter("pass"));
    	user.setName(request.getParameter("name"));

    	userService.signUpUser(user);
    	
        return view;
	}

	    
    @RequestMapping(value="/login.html", method = RequestMethod.GET)
    public ModelAndView logInHandleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Log.info("Returning login view");
//        Users u =test.getUser();
//        Log.info("-------------------------------"+u.getId());
    	ModelAndView view = new ModelAndView("html/loginPage");
    	
        return view;
	}
    
    @RequestMapping(value="/user/welcome.html", method = RequestMethod.POST)
    public ModelAndView loginPostHandleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Log.info("/user/welcome.html");
        ModelAndView view;
        User u =new User();
        String name = request.getParameter("name");
        String pas = request.getParameter("password");
        if(pas != null && name != null){
            u.setName(name.toString());
            u.setPassword(pas.toString());
            User existingUser = userService.getUserForNameAndPas(u);
            if(existingUser != null){        
            	request.getSession().setAttribute(loginUser, existingUser);
            	view = new ModelAndView("html/userWellcome");
            	view.addObject("recipeList", loadSuggestions(existingUser.getUserId()));
            	 return view;
            }
        }
       	view = new ModelAndView("html/loginPage");        	        
        return view;
	}

    
    @RequestMapping(value="/user/welcome.html", method = RequestMethod.GET)
	public ModelAndView userAlreadyLoggedin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Log.info("/user/welcome.html");
        ModelAndView view;
        User u =new User();
        String name = request.getParameter("name");
        String pas = request.getParameter("password");
        if(pas != null && name != null){
            u.setName(name.toString());
            u.setPassword(pas.toString());
            User existingUser = userService.getUserForNameAndPas(u);
            if(existingUser != null){        
            	request.getSession().setAttribute(loginUser, existingUser);
            	view = new ModelAndView("html/userWellcome");
            	view.addObject("recipeList", loadSuggestions(existingUser.getUserId()));
            	 return view;
            }
        }

        User userData = (User) request.getSession().getAttribute(loginUser);
    	if (userData!=null){
        	view = new ModelAndView("html/userWellcome");        	            		
        	view.addObject("recipeList", loadSuggestions(userData.getUserId()));
        	
        	return view;
    	}
    	view = new ModelAndView("html/loginPage");        	        
        return view;
	}
    
    @RequestMapping(value="/logOut.html", method = RequestMethod.GET)
	public ModelAndView userlogOut(HttpServletRequest request, HttpServletResponse response) {
    	
    	Log.info("Returning user welcome view");
    	request.getSession().setAttribute(loginUser,null);
    	request.getSession().removeAttribute(loginUser);

    	ModelAndView view = new ModelAndView("html/mainTemplate");
    	
    	ArrayList<Recipe> rList = recipeService.getLastRecipes(10,1); 
    	view.addObject("recipeList", rList);
        return view;
	}
    
    
    public ArrayList<Recipe> loadSuggestions(int userId){
    	
    	return recipeService.getSuggestions(userId);
    	
    }
}
