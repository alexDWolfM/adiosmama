package adios.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import adios.model.Recipe;
import adios.service.RecipeService;

@Controller
public class WellcomeController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private RecipeService recipeService;

	/**
	 * returns the welcome page with the last 5 recipe.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/welcome.html", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		logger.info("Returning welcome view without user is not login");
		ModelAndView view = new ModelAndView("html/mainTemplate");

		ArrayList<Recipe> rList = recipeService.getLastRecipes(10, 1);
		view.addObject("recipeList", rList);
		view.addObject("nextPage", 2);
		view.addObject("prevPage", 0);
		return view;
	}

	/**
	 * returns the welcome page with the last 5 recipe.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/page/{page}/welcome.html", method = RequestMethod.GET)
	public ModelAndView wellcomeWithPagination(@PathVariable int page,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (page < 0) {
			page = 1;
		}

		logger.info("Returning welcome view with page=" + page);
		ModelAndView view = new ModelAndView("html/mainTemplate");

		ArrayList<Recipe> rList = recipeService.getLastRecipes(10, page);
		view.addObject("recipeList", rList);
		int nextPage = page + 1;
		view.addObject("nextPage", nextPage);
		int prevPage = page - 1;
		view.addObject("prevPage", prevPage);
		return view;
	}

	// @RequestMapping("/helloworld.html")
	// public ModelAndView helloWord(){
	// String message = "Hello World, Spring 3.0!";
	// return new ModelAndView("index", "message",message);
	// }
	//
	//
	// // @RequestMapping(value="/{name}", method = RequestMethod.GET)
	// @RequestMapping(value="/welcome2/{name}.html")
	// public String getWelcome(@PathVariable String name, ModelMap model) {
	//
	// model.addAttribute("movie", name);
	// return "welcome";
	//
	// }
}
