package adios.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.Ingredient;
import adios.model.Instruction;
import adios.model.Tag;

import java.io.Serializable;
import java.util.ArrayList;

@Component
@Entity
public class IngredientDao{


    // Injected database connection:
    @PersistenceContext private EntityManager em;
 
    // Stores a new guest:
    @Transactional
    public void persist(Ingredient ing) {
        em.persist(ing);
    }
 
    /**
     * returns a given ingredient for a name
     * @param ing
     * @return
     */
    @Transactional
    public Ingredient getIngredientForName(Ingredient ing) {
    	TypedQuery<Ingredient> query = em.createQuery(
    			"SELECT i FROM Ingredient i  WHERE i.name = '"+ing.getName()+"'", Ingredient.class);    	
    	return query.getSingleResult();
    }
    
    
    /**
     * retunrs a given ingredient for a given Id
     * @param ing
     * @return
     */
    @Transactional
    public Ingredient getIngredientForId(int id) {
    	TypedQuery<Ingredient> query = em.createQuery(
    			"SELECT i FROM Ingredient i  WHERE i.ingredientId = "+id+"", Ingredient.class);
    	return query.getSingleResult();
    }
    
    
    
    /**
     * returns mos tused tags.
     * @param r
     * @return
     */
	@Transactional
	public ArrayList<Ingredient> getMostUsedIng(){

		TypedQuery<Ingredient> query = em.createQuery(
				"SELECT i FROM Ingredient i ", Ingredient.class).setFirstResult(0).setMaxResults(10);
		return (ArrayList<Ingredient>)query.getResultList();    	
	}
}
