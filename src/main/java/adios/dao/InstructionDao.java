package adios.dao;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.Instruction;
import adios.model.UserRole;


@Component
@Entity
public class InstructionDao {


    // Injected database connection:
    @PersistenceContext private EntityManager em;
 
    // Stores a new guest:
    @Transactional
    public void persist(Instruction inst) {
        em.persist(inst);
    }
 
    // Retrieves all the guests:
//    public UserRole getRoleForRoleId(UserRole role) {
//    	TypedQuery<UserRole> query = em.createQuery(
//            "SELECT r FROM UserRole r WHERE r.roleId = "+role.getRoleId()+"", UserRole.class);
//    	return query.getSingleResult();
//    }
}
