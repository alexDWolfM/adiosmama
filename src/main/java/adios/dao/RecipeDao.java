package adios.dao;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.Filter;
import adios.model.Ingredient;
import adios.model.Recipe;
import adios.model.UserRole;

@Component
@Entity
public class RecipeDao {

	// Injected database connection:
	@PersistenceContext
	private EntityManager em;

	protected final Log Log = LogFactory.getLog(getClass());

	/**
	 * persit a recipe
	 * 
	 * @param recipe
	 * @return
	 */
	@Transactional
	public boolean persist(Recipe recipe) {
		try {
			em.persist(recipe);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	/**
	 * return a recipe for a recipe Id
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public Recipe getRecipeForId(int id) {
		TypedQuery<Recipe> query = em.createQuery(
				"SELECT r FROM Recipe r WHERE r.recipeId = " + id + "",
				Recipe.class);
		return query.getSingleResult();

	}

	/**
	 * return a recipe for a user Id and a recipe name
	 * 
	 * @param userId
	 * @param name
	 * @return
	 */
	@Transactional
	public Recipe getRecipeForUserIdAndName(int userId, String name) {
		TypedQuery<Recipe> query = em.createQuery(
				"SELECT r FROM Recipe r WHERE r.userId = " + userId
						+ " AND r.name LIKE '" + name + "' ", Recipe.class);
		return query.getSingleResult();

	}

	/**
	 * returns all the recipes for a user name
	 * 
	 * @param userId
	 * @return
	 */
	@Transactional
	public ArrayList<Recipe> getRecipeForUserIdAndName(int userId) {
		TypedQuery<Recipe> query = em.createQuery(
				"SELECT r FROM Recipe r WHERE r.userId = " + userId + "",
				Recipe.class);
		return (ArrayList<Recipe>) query.getResultList();

	}

	/**
	 * returns all the recipes that contains the parameter value in theirs name.
	 * 
	 * @param text
	 * @return
	 */
	@Transactional
	public ArrayList<Recipe> getRecipeForName(String text) {
		TypedQuery<Recipe> query = em.createQuery(
				"SELECT r FROM Recipe r WHERE LOWER(r.name) LIKE '%" + text
						+ "%'", Recipe.class);
		return (ArrayList<Recipe>) query.getResultList();
	}

	// SELECT a.au_lname, a.au_fname, t.title
	// FROM authors a INNER JOIN titleauthor ta
	// ON a.au_id = ta.au_id JOIN titles t
	// ON ta.title_id = t.title_id
	// WHERE t.type = 'trad_cook'
	// ORDER BY t.title ASC

	// SELECT a.email FROM usuarios a INNER JOIN eventos b on a.user_id =
	// b.user_id INNER JOIN eventos_detalles c ON c.eventoid = b.eventoid WHERE
	// c.evento_detallesID = 2481
	/**
	 * returns all the recipes that contains the parameter value in theirs
	 * ingredients name.
	 * 
	 * @param text
	 * @return
	 */
	@Transactional
	public ArrayList<Recipe> getRecipeForIngredientName(String text) {
		String q = "SELECT r "
				+ "FROM Recipe r ,TableRecipeIngredient t, Ingredient i "
				+ "WHERE r.recipeId = t.recipeId "
				+ "AND i.ingredientId = t.ingredientId " + "AND UPPER(i.name) LIKE '%"
				+ text.toUpperCase() + "%'";
		TypedQuery<Recipe> query = em.createQuery(q, Recipe.class);
		return (ArrayList<Recipe>) query.getResultList();
	}

	/**
	 * updates instructions for a recipe
	 * 
	 * @param r
	 * @return
	 */
	@Transactional
	public boolean update(Recipe r) {
		String imageName = this.createImageName(r.getName(), r.getUserId());
		Recipe recipeDB = em.find(Recipe.class, r.getRecipeId());
		recipeDB.setInstructions(r.getInstructions());
		recipeDB.setName(r.getName());
		recipeDB.setDescription(r.getDescription());
		recipeDB.setExternalMedia(r.getExternalMedia());
		recipeDB.setImageName(r.getImageName());
		em.persist(recipeDB);
		return true;

	}

	/**
	 * updates a recipe
	 * 
	 * @param r
	 * @return
	 */
	@Transactional
	public int updateSimpleDataForRecipeForUserIdAndName(Recipe r) {
		String imageName = this.createImageName(r.getName(), r.getUserId());
		Query query = em.createQuery("UPDATE Recipe r " + "SET r.userId = "
				+ r.getUserId() + "," + "r.name = '" + r.getName() + "',"
				+ "r.description = '" + r.getDescription() + "',"
				+ "r.externalMedia = '" + r.getExternalMedia() + "',"
				+ "r.imageName = '" + imageName + "' " +
				// "r.instructions = '"+r.getInstructions()+"' "+
				"WHERE r.recipeId = " + r.getRecipeId() + "");

		return query.executeUpdate();

	}

	/**
	 * returns the last 10 recipes, 10 will be hardcoded, it should be moved to a
	 * properties files or a parameter, in an actaulization pagination has been added 
	 * 
	 * @param page
	 * @param pageSize
	 * @return
	 */
	@Transactional
	public ArrayList<Recipe> getLastRecipes(int pageSize, int page) {

		if(page < 1 ){
			page=1;
		}
		TypedQuery<Recipe> query = em
				.createQuery("SELECT r FROM Recipe r ORDER BY r.recipeId DESC",
						Recipe.class).setFirstResult(((pageSize)*(page))-pageSize);
		query.setMaxResults(pageSize);
		ArrayList<Recipe> rList = (ArrayList) query.getResultList();
		Log.info("rlist::::"+rList.get(0).getImageName());
		Log.info("rlist::::"+rList);
		if (rList!=null){
			Log.info("rlist::::"+rList.size());				
		}
		return rList;
	}

	/**
	 * Returns the recipes for a given filter-set. this function creates the
	 * reuired query.
	 * 
	 * 
	 * SELECT  tab.recipeId 
	 * FROM  TableRecipeIngredient tab, Ingredient ing, Tag tag 
	 * WHERE 
	 * tab.ingredientId=ing.ingredientId AND 
	 * tab.recipeId=tag.recipeId AND (
	 * ing.name LIKE '%pastel%' OR 
	 * ing.name LIKE '%salmon%'  ) AND ( 
	 * tag.name LIKE '%tag1%' OR 
	 * tag.name LIKE '%tag0%' )
	 * GROUP BY tab.recipeId 
	 * HAVING COUNT(tag.recipeId) > 3
	 * 
	 * @param pageSize
	 * @return
	 */
	@Transactional
	public ArrayList<Recipe> getRecipesForFIlters(
			ArrayList<Filter> filterListIng, ArrayList<Filter> filterListTag,
			String recipeName) {
		ArrayList<Recipe> result = new ArrayList<Recipe>();


		

		int size= filterListTag.size() + filterListIng.size() - 1;
		
		String whereIng = this.generetaANDForIngredients(filterListIng);
		String whereTag = this.generetaANDForTag(filterListTag);
		if (!whereTag.isEmpty() && !whereIng.isEmpty()) {
			whereIng = "( " + whereIng + " ) AND ( ";
			whereTag = whereTag +") " ;
		}
		String whereName="";
		if (recipeName != null || !recipeName.equals("")){
			if (!whereTag.isEmpty() || !whereIng.isEmpty()) {
				whereName=" AND r.name LIKE '%"+recipeName+"%' ";
			}else{
				whereName=" UPPER(r.name) LIKE '%"+recipeName.toUpperCase()+"%' ";
			}
		}
		
		String q = "SELECT DISTINCT tab.recipeId "
				+ "FROM  TableRecipeIngredient tab, Ingredient ing, Tag tag, Recipe r "
				+ "WHERE " 
				+ "tab.ingredientId=ing.ingredientId AND tag.recipeId=r.recipeId AND "
				+ "tab.recipeId=tag.recipeId AND " + whereIng + whereTag + whereName
				+ "GROUP BY tab.recipeId " 
				+ " HAVING COUNT(tag.recipeId) > "+size;
		
		Log.info("query::::"+q);
		Query query = em.createQuery(q);
		ArrayList<Integer> recipeIds = (ArrayList<Integer>) query
				.getResultList();

		Iterator<Integer> ingId = recipeIds.iterator();
		while (ingId.hasNext()) {
			int i = ingId.next();
			Recipe recipe = this.getRecipeForId(i);
			result.add(recipe);
			Log.info("recipe name for filter :: " + recipe.getName());
		}

		return result;
	}

	/**
	 * Returns the and clause for a list of Ingredients
	 * 
	 * * @return
	 */

	public String generetaANDForIngredients(ArrayList<Filter> filterList) {
		String where = "";
		for (int i = 0; i < filterList.size(); i++) {
			Filter f = filterList.get(i);
			String or = " UPPER(ing.name) LIKE '%" + f.getId().toUpperCase() + "%' ";
			if ((i + 1) < filterList.size()) {
				or = or + "OR";
			}
			where = where + or;
		}
		return where;
	}

	/**
	 * Returns the and clause for a list of tags
	 * 
	 * * @return
	 */

	public String generetaANDForTag(ArrayList<Filter> filterList) {
		String where = "";
		for (int i = 0; i < filterList.size(); i++) {
			Filter f = filterList.get(i);
			String or = " UPPER(tag.name) LIKE '%" + f.getId().toUpperCase() + "%' ";
			if ((i + 1) < filterList.size()) {
				or = or + "OR";
			}
			where = where + or;
		}
		return where;
	}

	/**
	 * generates the name for the image of a recipe
	 * 
	 * @param name
	 * @param userId
	 * @return
	 */
	public String createImageName(String name, int userId) {
		String imageName = userId + name.replace(" ", "");
		imageName = imageName + "";
		return imageName;
	}
	
	
	/**
	 * returns two suggestions for a user,
	 * the first suggestion is based on the most used ingredients and 
	 * the second one based on the most used tags
	 * @param userId
	 * @return
	 */
	public ArrayList<Recipe> suggestRecipe(int userId){
		ArrayList<Recipe> result = new ArrayList<Recipe>();
//		String q= "SELECT  t.ingredientId " +
//				"FROM  TableRecipeIngredient t " +
//				"GROUP BY t.ingredientId " +
//				"ORDER BY COUNT(t) DESC "

		String q= " SELECT  t.ingredientId  "
    	+" FROM  Recipe r, TableRecipeIngredient t  "
    	+" WHERE r.recipeId=t.recipeId  "
    	+" AND r.userId="+userId
    	+" GROUP BY t.ingredientId  "
    	+" ORDER BY COUNT(t.ingredientId) DESC";

		Query query = em.createQuery(q);
		query.setMaxResults(2);
    	ArrayList<Integer> mostUsedIngIdForUser = (ArrayList<Integer>) query.getResultList();
    	
    	if(mostUsedIngIdForUser.size()<2){
    		return result;
    	}
    	
    	q="SELECT  r.recipeId"
    	+" FROM  Recipe r, TableRecipeIngredient t" 
    	+" WHERE (t.ingredientId="+mostUsedIngIdForUser.get(0).intValue()
    	+" OR t.ingredientId="+mostUsedIngIdForUser.get(1).intValue()+")"
    	+" AND r.recipeId=t.recipeId" 
    	+" GROUP BY r.recipeId "
    	+" ORDER BY COUNT(r.recipeId) DESC";

    	query = em.createQuery(q);
		query.setMaxResults(2);
    	ArrayList<Integer> recipeIdForIngredients = (ArrayList<Integer>) query.getResultList();
    	result.add(this.getRecipeForId(recipeIdForIngredients.get(0).intValue()));
    	result.add(this.getRecipeForId(recipeIdForIngredients.get(1).intValue()));
//    	
    	Log.info("recipeIdForIngredients[0]="+((Integer)recipeIdForIngredients.indexOf(0)).intValue());
    	Log.info("recipeIdForIngredients[1]="+((Integer)recipeIdForIngredients.indexOf(1)).intValue());
    	Log.info("recipeIdForIngredients[0]="+result.get(0).getName());
    	Log.info("recipeIdForIngredients[1]="+result.get(1).getName());
//    	

    	q="SELECT  t.name"
    	+" FROM  Recipe r, Tag t "
    	+" WHERE r.recipeId=t.recipeId" 
    	+" AND r.userId="+userId
    	+" GROUP BY t.name "
    	+" ORDER BY COUNT(t.name) DESC";

		query = em.createQuery(q);
		query.setMaxResults(2);
    	ArrayList<String> mostUsedTagIdForUser = (ArrayList<String>) query.getResultList();
    	

    	q="SELECT  r.recipeId"
    	+" FROM  Recipe r, Tag t" 
    	+" WHERE (t.name LIKE '"+mostUsedTagIdForUser.get(0)+"'"
    	+" OR t.name LIKE '"+mostUsedTagIdForUser.get(1)+"')"
    	+" AND r.recipeId=t.recipeId ";
    	
    	query = em.createQuery(q);
		query.setMaxResults(2);
    	ArrayList<Integer> recipeIdForTag = (ArrayList<Integer>) query.getResultList();
    	result.add(this.getRecipeForId(recipeIdForTag.get(0).intValue()));
    	result.add(this.getRecipeForId(recipeIdForTag.get(1).intValue()));
    	
    	
    	

    	return result;
	}

	// Retrieves all the guests:
	// public UserRole getRoleForRoleId(UserRole role) {
	// TypedQuery<UserRole> query = em.createQuery(
	// "SELECT r FROM UserRole r WHERE r.roleId = "+role.getRoleId()+"",
	// UserRole.class);
	// return query.getSingleResult();
	// }
}