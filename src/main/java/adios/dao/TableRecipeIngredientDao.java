package adios.dao;

import java.util.ArrayList;
import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.Filter;
import adios.model.Ingredient;
import adios.model.Ingredient4Recipe;
import adios.model.Recipe;
import adios.model.TableRecipeIngredient;
import javax.persistence.Entity;


@Component
@Entity
public class TableRecipeIngredientDao {

    // Injected database connection:
    @PersistenceContext private EntityManager em;
    
	@Autowired
	private IngredientDao ingredientDao;
	@Autowired
	private RecipeDao recipeDao;
	
	/**
     * saves the relation from a ingredient and a given recipe
     * @param tab
     */
    @Transactional
    public void persist(TableRecipeIngredient tab) {
        em.persist(tab);
    }
    
    
    /**
     * return an ingredient for  given  ingredient and recipe
     * @param tab
     */
    @Transactional
    public ArrayList<TableRecipeIngredient> find(TableRecipeIngredient tab) {
    	TypedQuery<TableRecipeIngredient> query = em.createQuery(
    			"SELECT t FROM TableRecipeIngredient t  WHERE t.ingredientId = "+tab.getIngredientId()+" AND t.recipeId = "+tab.getRecipeId()+"", TableRecipeIngredient.class);
    	return  (ArrayList<TableRecipeIngredient>) query.getResultList();
    }
    
    /**
     * delete the relation from a ingredient and a given recipe
     * @param tab
     */
    @Transactional
    public void remove(TableRecipeIngredient tab) {
    	em.remove(tab);
    }
    
    /**
     * returns all the ingredients for a recipe
     * @param i4r
     * @return
     */
    @Transactional
    public ArrayList<TableRecipeIngredient> getIngredientsForRecipe(Ingredient4Recipe i4r) {
    	TypedQuery<TableRecipeIngredient> query = em.createQuery(
    			"SELECT t FROM TableRecipeIngredient t  WHERE t.recipeId = "+i4r.getRecipeId()+"", TableRecipeIngredient.class);
    	return  (ArrayList<TableRecipeIngredient>) query.getResultList();
    }
    
    
    /**
     * get 10 ingredients, the most used.
     * I expected to run this query:
     * SELECT  t.ingredientId
     *	FROM Ingredient i, TableRecipeIngredient t
	*	WHERE i.ingredientId = t.ingredientId
	*	GROUP BY t.ingredientId
	*	ORDER BY COUNT(t) DESC 
	* sadly the DB has problems with this query. 
	* Thats the reason the code is not running in a  optimized way.
     * @return
     */
	@Transactional
	public ArrayList<Ingredient> getMostUsedIng(){

		ArrayList<Ingredient> result = new ArrayList<Ingredient>();
		String q= "SELECT  t.ingredientId " +
				"FROM  TableRecipeIngredient t " +
				"GROUP BY t.ingredientId " +
				"ORDER BY COUNT(t) DESC ";
		Query query = em.createQuery(q);
		query.setMaxResults(10);
    	ArrayList<Integer> ingredientIds = (ArrayList<Integer>) query.getResultList();

    	Iterator<Integer> ingId = ingredientIds.iterator();
    	while(ingId.hasNext()){
    		int i =  ingId.next();
    		Ingredient ing = ingredientDao.getIngredientForId(i);
    		result.add(ing);
    	}
    	
    	return result;
	}
	
    /**
     * delete the relation between a given Ingredient and recipe
     * @param recipeId
     * @return
     */
	@Transactional
	public boolean delteRelationForRecipeAndIngredient(int recipeId){

		String q= "DELETE FROM TableRecipeIngredient t WHERE t.recipeId ="+recipeId;
		Query query = em.createQuery(q);
		int changed=query.executeUpdate();
		System.out.println("recipeId:::"+recipeId+"  entities afected by delteRelationForRecipeAndIngredient:::"+changed);
		
    	return true;
	}
	
}