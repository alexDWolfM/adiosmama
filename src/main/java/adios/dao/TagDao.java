package adios.dao;


import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.Recipe;
import adios.model.Tag;
import adios.model.User;



@Component
@Entity
public class TagDao{


    // Injected database connection:
    @PersistenceContext private EntityManager em;
 
  
    /**
     * creates a new tag
     * @param tag
     * @return
     */
    @Transactional
    public boolean addTag(Tag tag) {
        em.persist(tag);
        return true;
    }
    
    
    /**
     * remove an existing tag
     * @param t
     * @return
     */
    @Transactional
	public boolean removeTag(Tag t){
    	Tag tagDB = em.find(Tag.class, t.getTagId());
    	em.remove(tagDB);
		return true;	    	
	    }


    /**
     * returns all the tags for a given recipe Id.
     * @param r
     * @return
     */
	@Transactional
	public ArrayList<Tag> getTagForRecipe(Recipe r){

		TypedQuery<Tag> query = em.createQuery(
	      "SELECT t FROM Tag t WHERE t.recipeId = "+r.getRecipeId()+"", Tag.class);
		return (ArrayList<Tag>)query.getResultList();    	
	}
    
	
    /**
     * returns mos tused tags.
     * @param r
     * @return
     */
	@Transactional
	public ArrayList<Tag> getMostUsedTags(){
		
		TypedQuery<Tag> query = em.createQuery(
				 "SELECT t.name FROM Tag t GROUP BY t.name ORDER BY COUNT(t.name) desc", Tag.class).setFirstResult(0).setMaxResults(10);
		return (ArrayList<Tag>)query.getResultList();    	
	}
	
    /**
     * delete a tag or a given recipeId
     * @param recipeId
     * @param ingredientId
     * @return
     */
	@Transactional
	public boolean delteTagForRecipeId(int recipeId){

		String q= "DELETE FROM Tag t WHERE t.recipeId ="+recipeId;
		Query query = em.createQuery(q);
		int changed=query.executeUpdate();
		System.out.println("recipeId:::"+recipeId+"  entities afected by delteTagForRecipeId:::"+changed);

    	return true;
	}


	
	
	
	
    
    // Retrieves all the guests:
//    public UserRole getRoleForRoleId(UserRole role) {
//    	TypedQuery<UserRole> query = em.createQuery(
//            "SELECT r FROM UserRole r WHERE r.roleId = "+role.getRoleId()+"", UserRole.class);
//    	return query.getSingleResult();
//    }
}