package adios.dao;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.Ingredient;
import adios.model.Recipe;
import adios.model.User;

import java.io.Serializable;


@Component
@Entity
public class UserDao{


    // Injected database connection:
    @PersistenceContext private EntityManager em;
 
    /**
     *  Stores a new User
     * @param user
     * @return
     */
    @Transactional
    public boolean persist(User user) {
    	try{
            em.persist(user);    	
            return true;
    	}catch(Exception ex){
    		return false;
    	}
    }
    
    /**
     * Updates a User and persist it on the data base.
     * @param u
     * @return
     */
    @Transactional
    public User updateUser(User u) {
    	User userDb = em.find(User.class, u.getUserId());
  	  	userDb.setAuthority(u.getAuthority());
  	  	userDb.setFavoriteRecipes(u.getFavoriteRecipes());
  	  	userDb.setCartRecipes(u.getCartRecipes());	  	
  	  	userDb.setMail(u.getMail());
  		userDb.setName(u.getName());
  		userDb.setPassword(u.getPassword());
    	em.persist(userDb);
  	  	return userDb;
    }
    
    /**
     * gets a user for the mail
     * @param mail
     * @return
     */
    public User getUserForMail(String mail){
//    	mail=mail.replace("@", "\@");
    TypedQuery<User> query = em.createQuery(
      "SELECT u FROM User u WHERE u.mail = '"+mail+"'", User.class);
	return query.getSingleResult();    	
    }

    /**
     * return a user for a given user name and password
     * @param name
     * @param password
     * @return
     */
    public User getUserForNameAndPas(String name, String password){
//    	mail=mail.replace("@", "\@");
    TypedQuery<User> query = em.createQuery(
      "SELECT u FROM User u WHERE u.name = '"+name+"' AND u.password = '"+password+"'", User.class);
	return query.getSingleResult();    	
    }
    
    /**
     * return a user for a given user name
     * @param mail
     * @return
     */
    public User getUserForName(String name){
//    	mail=mail.replace("@", "\@");
    TypedQuery<User> query = em.createQuery(
      "SELECT u FROM User u WHERE u.name = '"+name+"'", User.class);
	return query.getSingleResult();    	
    }
    
    // Retrieves all the guests:
//    public UserRole getRoleForRoleId(UserRole role) {
//    	TypedQuery<UserRole> query = em.createQuery(
//            "SELECT r FROM UserRole r WHERE r.roleId = "+role.getRoleId()+"", UserRole.class);
//    	return query.getSingleResult();
//    }
}