package adios.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import adios.model.UserRole;
import javax.persistence.Entity;

@Component
@Entity
public class UserRoleDao {

    // Injected database connection:
    @PersistenceContext private EntityManager em;
 
    // Stores a new guest:
    @Transactional
    public void persist(UserRole role) {
        em.persist(role);
    }
 
    // Retrieves all the guests:
    public UserRole getRoleForRoleId(UserRole role) {
    	TypedQuery<UserRole> query = em.createQuery(
            "SELECT r FROM UserRole r WHERE r.roleId = "+role.getRoleId()+"", UserRole.class);
    	return query.getSingleResult();
    }
}



