package adios.model;


/**
 * This classs is only used for front end information return, it contains all the important info needed in the view layer.
 * @author wolf
 *
 */
public class Ingredient4Recipe {

	private int recipeId;
	private int ingrdientId;
	private int quantity;
	private String name;	
	private String unit;
	
	
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	public int getIngrdientId() {
		return ingrdientId;
	}
	public void setIngrdientId(int ingrdientId) {
		this.ingrdientId = ingrdientId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
	
}
