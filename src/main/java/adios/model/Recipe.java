package adios.model;


import javax.persistence.Column;
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.jdo.annotations.Indices;
import javax.jdo.annotations.Uniques;
import javax.jdo.annotations.Index;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;


@Entity
@Indices({
    @Index(members={"name","userId"}, unique="true")
})
public class Recipe implements Serializable {
    private static final long serialVersionUID = 1L;

    // Persistent Fields:
    @Id @GeneratedValue
	private int recipeId;
	private String name;
	private String description;	
	private String externalMedia;
	private String imageName;
	private int userId;
	@Basic
	ArrayList<String> instructions;
	
	public Recipe() {
	}

	

	public Recipe(String name, String description, String externalMedia,String imageName,
			int userId, ArrayList<String> instructions) {
		this.name = name;
		this.description = description;
		this.externalMedia = externalMedia;
		this.imageName=imageName;
		this.userId = userId;
		this.instructions = instructions;
	}



	public ArrayList<String> getInstructions() {
		return instructions;
	}



	public void setInstructions(ArrayList<String> instructions) {
		this.instructions = instructions;
	}



	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExternalMedia() {
		return externalMedia;
	}

	public void setExternalMedia(String externalMedia) {
		this.externalMedia = externalMedia;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

}
