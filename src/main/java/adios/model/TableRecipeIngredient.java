package adios.model;

import java.io.Serializable;

import javax.jdo.annotations.Index;
import javax.jdo.annotations.Indices;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Indices({
    @Index(members={"ingredientId","recipeId"}, unique="true")
})
public class TableRecipeIngredient implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // Persistent Fields:
    @Id @GeneratedValue
    private int id;
	private int ingredientId;
    private int recipeId;
	private String ingUnit ;
	private int ingQuantity;
    
	public TableRecipeIngredient() {
	}
	

	public TableRecipeIngredient( int ingredientId, int recipeId,
			String ingUnit, int ingQuantity) {
		super();
		this.ingredientId = ingredientId;
		this.recipeId = recipeId;
		this.ingUnit = ingUnit;
		this.ingQuantity = ingQuantity;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIngredientId() {
		return ingredientId;
	}
	public void setIngredientId(int ingredientId) {
		this.ingredientId = ingredientId;
	}
	public int getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public String getIngUnit() {
		return ingUnit;
	}

	public void setIngUnit(String ingUnit) {
		this.ingUnit = ingUnit;
	}

	public int getIngQuantity() {
		return ingQuantity;
	}

	public void setIngQuantity(int ingQuantity) {
		this.ingQuantity = ingQuantity;
	}

	
}
