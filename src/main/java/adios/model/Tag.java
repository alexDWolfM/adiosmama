package adios.model;


import java.io.Serializable;

import javax.jdo.annotations.Index;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;



@Entity
@Index(members={"recipeId","name"})
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // Persistent Fields:
    @Id @GeneratedValue
	private int tagId;
    private int recipeId;
	private String name;

	
	
	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}
	
	public Tag(String name) {

		this.name = name;
	}

	public Tag() {
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}





}
