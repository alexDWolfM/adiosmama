package adios.model;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;

import javax.jdo.annotations.Unique;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
public class User implements Serializable {
	 private static final long serialVersionUID = 1L;
	  
    @Id @GeneratedValue
	private int userId;

	@Unique private String name;
	private String password;
	private boolean enabled;
	@Unique private String mail;
	private String authority;
	@Basic 
	private HashMap<Integer,Recipe> favoriteRecipes;

	@Basic 
	private HashMap<Integer,Recipe> cartRecipes;
	
	
	public User() {
	}


	public User(int userId, String name, String password, boolean enabled,
			String mail, String authority,
			HashMap<Integer, Recipe> favoriteRecipes,
			HashMap<Integer, Recipe> cartRecipes) {
		super();
		this.userId = userId;
		this.name = name;
		this.password = password;
		this.enabled = enabled;
		this.mail = mail;
		this.authority = authority;
		this.favoriteRecipes = favoriteRecipes;
		this.cartRecipes = cartRecipes;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public boolean isEnabled() {
		return enabled;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getAuthority() {
		return authority;
	}


	public void setAuthority(String authority) {
		this.authority = authority;
	}


	public HashMap<Integer, Recipe> getFavoriteRecipes() {
		return favoriteRecipes;
	}


	public void setFavoriteRecipes(HashMap<Integer, Recipe> favoriteRecipes) {
		this.favoriteRecipes = favoriteRecipes;
	}


	public HashMap<Integer, Recipe> getCartRecipes() {
		return cartRecipes;
	}


	public void setCartRecipes(HashMap<Integer, Recipe> cartRecipes) {
		this.cartRecipes = cartRecipes;
	}
	

}
