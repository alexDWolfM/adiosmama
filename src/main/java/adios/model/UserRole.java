package adios.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;



@Entity
public class UserRole  implements Serializable {
    private static final long serialVersionUID = 1L;
    
    // Persistent Fields:
    @Id @GeneratedValue
    int roleId;
    private String AUTHORITY;
    
    
    public UserRole() {
    }
    
	public UserRole( String aUTHORITY) {
		AUTHORITY = aUTHORITY;
	}
    
    
    // String Representation:
    @Override
    public String toString() {
        return AUTHORITY + " (with  id " + roleId + ")";
    }
    
    public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int id) {
		this.roleId= id;
	}


	public String getAUTHORITY() {
		return AUTHORITY;
	}

	public void setAUTHORITY(String aUTHORITY) {
		AUTHORITY = aUTHORITY;
	}


}
