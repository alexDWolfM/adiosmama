package adios.service;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adios.dao.IngredientDao;
import adios.dao.RecipeDao;
import adios.dao.TableRecipeIngredientDao;
import adios.model.Ingredient;
import adios.model.Ingredient4Recipe;
import adios.model.Recipe;
import adios.model.TableRecipeIngredient;


@Service("IngredientService")
public class IngredientService {

	protected final Log Log = LogFactory.getLog(getClass());
		 
	@Autowired
	private IngredientDao ingredientDao;
	
	@Autowired
	private TableRecipeIngredientDao tableRecipeIngredientDao;
	
	
	public boolean addIngredient (Ingredient4Recipe i4r){
		Ingredient i = new Ingredient();
		i.setName(i4r.getName());
		try{
			i = ingredientDao.getIngredientForName(i);			
		}catch (Exception e){
			ingredientDao.persist(i);
			i=ingredientDao.getIngredientForName(i);
		}finally{
			i4r.setIngrdientId(i.getIngredientId());
		}
		
		TableRecipeIngredient tab= new TableRecipeIngredient();
		tab.setRecipeId(i4r.getRecipeId());
		tab.setIngredientId(i4r.getIngrdientId());
		tab.setIngQuantity(i4r.getQuantity());
		tab.setIngUnit(i4r.getUnit());
		
		tableRecipeIngredientDao.persist(tab);
		
		return true;
	}
	
	public boolean delteRelationForRecipeAndIngredient(int recipeId, int ingredientId){
		tableRecipeIngredientDao.delteRelationForRecipeAndIngredient(recipeId);
		return true;
	}
	
	public ArrayList<Ingredient4Recipe> getIngredientsForRecipe(Ingredient4Recipe i4r)
	{
		ArrayList<TableRecipeIngredient> ingredientTableList =  tableRecipeIngredientDao.getIngredientsForRecipe(i4r);
		
		ArrayList<Ingredient4Recipe> i4rList = new ArrayList<Ingredient4Recipe>();
		if(ingredientTableList != null){
			Iterator<TableRecipeIngredient> it = ingredientTableList.iterator();
			TableRecipeIngredient tab=null;
			Ingredient i =null;
			while(it.hasNext()){
				Ingredient4Recipe i4rResult = new Ingredient4Recipe();
				tab = it.next();
				i = ingredientDao.getIngredientForId(tab.getIngredientId());
				i4rResult.setName(i.getName());
				i4rResult.setQuantity(tab.getIngQuantity());
				i4rResult.setUnit(tab.getIngUnit());
				
				i4rList.add(i4rResult);
			}
		}
		
		return i4rList;	
	}
	
	
	
	
	
	public ArrayList<Ingredient4Recipe> getMostUsedIng4Recipe()
	{
		
		ArrayList<Ingredient4Recipe> i4rList = new ArrayList<Ingredient4Recipe>();

		ArrayList<Ingredient> ingList = tableRecipeIngredientDao.getMostUsedIng();
		Iterator<Ingredient> it =  ingList.iterator();
		Ingredient i =null;
		
		while(it.hasNext()){
			Ingredient4Recipe i4rResult = new Ingredient4Recipe();
			i = it.next();			
			i4rResult.setName(i.getName());
			i4rResult.setIngrdientId(i.getIngredientId());
			i4rList.add(i4rResult);
		}
		
		return i4rList;	
	}
	
	
	 public boolean removeIngredientForRecipe(Ingredient i, Recipe r){
		return true;
	}
	
}
