package adios.service;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import adios.dao.RecipeDao;
import adios.dao.TableRecipeIngredientDao;
import adios.dao.TagDao;
import adios.model.Filter;
import adios.model.Recipe;
import adios.model.TableRecipeIngredient;



@Service("RecipeService")
public class RecipeService {

	 protected final Log Log = LogFactory.getLog(getClass());
	 
		@Autowired
		private RecipeDao recipeDao;
		
		@Autowired
		private TableRecipeIngredientDao recipeIngDao;
		
		@Autowired
		private TagDao tagDao;
		
		/**
		 * returns a recipe for a recipe Id
		 * @param id
		 * @return
		 */
		public Recipe getRecipe(int id){
			Recipe r=null;
			try{
				r =	recipeDao.getRecipeForId(id);
				
			}catch(Exception ex){
				Log.info("ERROR ::: getting Recipe!!!");
				return null;
			}
			return r;
		}
		
		
		/**
		 * return the last recipes
		 * @param pageSize
		 * @return
		 */
		public ArrayList<Recipe> getLastRecipes(int pageSize, int page){
			ArrayList<Recipe> rList=null;
			try{
			
			rList=recipeDao.getLastRecipes(pageSize, page);
			
		}catch(Exception ex){
			Log.info("ERROR ::: getting Recipe!!!");
			return null;
		}
		return rList;
		}
		
		
		public boolean deleteSecondaryInfo(int recipeId){
			
			Log.info("delete tag and ing for recipId:::"+recipeId);
			recipeIngDao.delteRelationForRecipeAndIngredient(recipeId);
			tagDao.delteTagForRecipeId(recipeId);
			return true;
		}
		/**
		 * update a recipe, image name is generated dinamicly on the dao layer
		 * @param r
		 * @return
		 */
		public boolean updateRecipe(Recipe r){
			try{
				recipeDao.update(r);
			}catch(Exception e){
				Log.error("!!!!!! Unexpected errorsaving recipe !!!!!1 cause ::::"+e.getCause());
				return false;
			}	
			return true;
		}

		/**
		 * Create a recipe and return the created recipe, getting it from the database
		 * @param r
		 * @return
		 */
		public Recipe createRecipe(Recipe r){
			try{
				recipeDao.persist(r);
				r = recipeDao.getRecipeForUserIdAndName(r.getUserId(), r.getName());			
			}catch(Exception ex){
				Log.info("WARNING ::: recipe already in db");
				Recipe oldRecipe = recipeDao.getRecipeForUserIdAndName(r.getUserId(), r.getName());
				r.setRecipeId(oldRecipe.getRecipeId());
				recipeDao.update(r);
			}			
			return r;
		}
		
	    
	    /**
	     * returns all the recipes that contains the parameter value in theirs name.
	     * @param text
	     * @return
	     */
	    public ArrayList<Recipe> getRecipeForName(String text){
	    	return recipeDao.getRecipeForName(text);
	    }
	    
	    
	    /**
	     * returns all the recipes that contains the parameter value in theirs ingredients name.
	     * @param text
	     * @return
	     */
	    public ArrayList<Recipe> getRecipeForIngredientName(String text){
	    	return recipeDao.getRecipeForIngredientName(text);
	    }
	    

	    
	    /**
	     * returns all the recipes for a given Filter Set. If the filter set is empty this function will return the last 10 introduced recipes.
	     * @param text
	     * @return
	     */
	    public ArrayList<Recipe> getRecipeForFilter(ArrayList<Filter> filters){
	    	ArrayList<Recipe> result = new ArrayList<Recipe>();
			if (filters.size()==0){
				recipeDao.getLastRecipes(100, 1);
			}else{
				ArrayList<Filter> ingList = new ArrayList<Filter>(); 
				ArrayList<Filter> tagList = new ArrayList<Filter>(); 
				Filter titleFilter = new Filter();
				
				for(Filter f:filters){
					if(f.getType().equals("ing")){
						ingList.add(f);
					}else if(f.getType().equals("tag")){
						tagList.add(f);
					}else{
						titleFilter=f;
					}
				}
				String title = "";
				if (titleFilter!= null && titleFilter.getId()!= null){
					title = titleFilter.getId();
				}		
				result = recipeDao.getRecipesForFIlters(ingList,tagList, title);
			}
			Log.info("result size for filters:::::"+result.size());
	    	return result;
	    }
	    
	    
		/**
		 * returns a recipe for a given user id
		 * @param userId
		 * @return
		 */
		public ArrayList<Recipe> getRecipeForUserId(int userId){
			ArrayList<Recipe> rList = recipeDao.getRecipeForUserIdAndName(userId);
		return rList;	
		}
		
		
		/**
		 * returns 4 suggestions for a user 2 based on ingredients and 2 based on tags
		 * @param userId
		 * @return
		 */
		public ArrayList<Recipe> getSuggestions(int userId){
		
			try{		
				return recipeDao.suggestRecipe(userId);
			}catch(Exception e){	
				return new ArrayList<Recipe>();	
			}
			
		}
}
