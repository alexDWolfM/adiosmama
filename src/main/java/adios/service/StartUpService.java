package adios.service;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adios.dao.RecipeDao;
import adios.dao.TagDao;
import adios.dao.UserDao;
import adios.dao.UserRoleDao;
import adios.model.Ingredient;
import adios.model.Ingredient4Recipe;
import adios.model.Recipe;
import adios.model.Tag;
import adios.model.User;
import adios.model.UserRole;

@Service("StartUp")
public class StartUpService {

	@Autowired
	private UserRoleDao userRoleDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private IngredientService ingredientService;

	@Autowired
	private RecipeDao recipeDao;

	@Autowired
	private TagDao tagService;

	private static final Log Log = LogFactory.getLog(UserRole.class);

	public boolean start() {
		Log.info("StartUp Serice: filling up data");

		User u;

		try {
			Log.info(":::  insert user roles");
			UserRole role = new UserRole("ROLE_ADMIN");
			userRoleDao.persist(role);
			role = new UserRole("ROLE_USER");
			userRoleDao.persist(role);
		} catch (Exception es) {
			Log.error("exception, already in database");
		}

		try {
			Log.info(":::  insert user (admin/admin , wolf/wolf)");
			u = new User(0, "admin", "admin", true, "admin@admin.com",
					"ROLE_ADMIN", null, null);
			userDao.persist(u);
			u = new User(0, "wolf", "wolf", true, "wolf@wolf.com", "ROLE_USER",
					null, null);
			userDao.persist(u);
		} catch (Exception es) {
			Log.error("exception, already in database");
		}

		try {
			Log.info(":::  insert Recipes ");
			String name = "Pa amb Tomaquet";
			String desc = "El pan con tomate (pa amb tomàquet, pa amb tomata o pa amb oli en los distintos dialectos de la lengua catalana) es una típica receta de la cocina aragonesa, balear, catalana, similar a la bruschetta al pomodoro italiana. Está considerado como uno de los mejores ejemplos que definen la dieta mediterránea.";
			String externalMedia = "<iframe src='http://player.vimeo.com/video/19862257' width='500' height='281' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> <p><a href='http://vimeo.com/19862257'>less is more | pa amb tomaquet | chleb z pomidorem</a> from <a href='http://vimeo.com/user2552887'>stokilo</a> on <a href='http://vimeo.com'>Vimeo</a>.</p>";
			u = userDao.getUserForMail("wolf@wolf.com");
			int userId = u.getUserId();
			String imageName = u.getUserId() + name.replace(" ", "");
			imageName = "http://b.geolocation.ws/img/002/965/017-F.jpg";

			ArrayList<String> instructions = new ArrayList();
			instructions
					.add("Se elabora frotando tomate crudo y maduro sobre una rebanada de pan, preferiblemente de pagès (pan de payés)");
			instructions.add("Coratar los tomates por la arg1 mitad");
			instructions.add("Untar el tomate");
			instructions.add("Aliñado al gusto con sal y aceite de oliva");

			Recipe recip = new Recipe(name, desc, externalMedia, imageName,
					userId, instructions);

			recipeDao.persist(recip);

			Log.info("recip id::: ");
			Log.info("recip id::: " + recip.getRecipeId());

			Tag t = new Tag();
			t.setName("Panecillos");
			t.setRecipeId(recip.getRecipeId());
			tagService.addTag(t);
			Tag t2 = new Tag();
			t2.setName("Bocatas");
			t2.setRecipeId(recip.getRecipeId());
			tagService.addTag(t2);

			Ingredient4Recipe i = new Ingredient4Recipe();
			i.setQuantity(100);
			i.setName("Pan");
			i.setRecipeId(recip.getRecipeId());
			i.setUnit("gr");
			ingredientService.addIngredient(i);

			i = new Ingredient4Recipe();
			i.setQuantity(100);
			i.setName("Tomate");
			i.setRecipeId(recip.getRecipeId());
			i.setUnit("gr");
			ingredientService.addIngredient(i);

			i = new Ingredient4Recipe();
			i.setQuantity(10);
			i.setName("A.O.V.E.");
			i.setUnit("cl");
			i.setRecipeId(recip.getRecipeId());
			ingredientService.addIngredient(i);

			name = name + "2";

			imageName = "http://www.foodrepublic.com/sites/default/files/imagecache/enlarge/whattoeat_lunch_paambtomaquet_0.jpg";

			Recipe recip2 = new Recipe(name, desc, externalMedia, imageName,
					userId, instructions);
			recipeDao.persist(recip2);

			Tag t3 = new Tag();
			t3.setName("Bocatas");
			t3.setRecipeId(recip2.getRecipeId());
			tagService.addTag(t3);
			Tag t4 = new Tag();
			t4.setName("facil");
			t4.setRecipeId(recip2.getRecipeId());
			tagService.addTag(t4);

			i = new Ingredient4Recipe();
			i.setQuantity(102);
			i.setName("Pan Pages");
			i.setRecipeId(recip2.getRecipeId());
			i.setUnit("gr");
			ingredientService.addIngredient(i);

			i = new Ingredient4Recipe();
			i.setQuantity(102);
			i.setName("Pan");
			i.setRecipeId(recip2.getRecipeId());
			i.setUnit("gr");
			ingredientService.addIngredient(i);

			i = new Ingredient4Recipe();
			i.setQuantity(102);
			i.setName("Tomate Untar");
			i.setRecipeId(recip2.getRecipeId());
			i.setUnit("gr");
			ingredientService.addIngredient(i);

			i = new Ingredient4Recipe();
			i.setQuantity(12);
			i.setName("Aceite Oliva V.E.");
			i.setUnit("cl");
			i.setRecipeId(recip2.getRecipeId());
			ingredientService.addIngredient(i);

			Log.info("recip id::: " + recip.getRecipeId());

		} catch (Exception es) {
			Log.error("exception, already in database");
			Log.error("exception", es);
		}

		return true;
	}

	public boolean fillUpOne() {

		return true;
	}

	public boolean fillUpTwo() {

		return true;
	}
}
