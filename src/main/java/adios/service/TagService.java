package adios.service;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adios.dao.RecipeDao;
import adios.dao.TagDao;
import adios.model.Recipe;
import adios.model.Tag;

@Service("TagService")
public class TagService {

	 protected final Log Log = LogFactory.getLog(getClass());
	 
		@Autowired
		private TagDao tagDao;
		
		
		public boolean addTag(Tag t){
			tagDao.addTag(t);
			
			return true;
		}

		public boolean removeTag(Tag t){
			tagDao.removeTag(t);
			return true;
		}
		
		public ArrayList<Tag> getTagForRecipe(Recipe r){
			ArrayList<Tag> tList = tagDao.getTagForRecipe(r);
			return tList;
		}
		
		
		public ArrayList<Tag> getMostUsedTags(){
			ArrayList<Tag> tList = tagDao.getMostUsedTags();
			return tList;
		}
		
		
		
}



