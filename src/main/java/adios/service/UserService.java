package adios.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import adios.dao.UserDao;
import org.springframework.stereotype.Service;

import adios.dao.RecipeDao;
import adios.model.Recipe;
import adios.model.User;



@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
	/**
	 * Create a user
	 * @param user
	 * @return
	 */
	public boolean signUpUser(User user){
		Object o1 = null;
		Object o2 = null;
		try{
			o1 =userDao.getUserForMail(user.getMail());
		}catch(Exception e){}
		
		try{o2 = userDao.getUserForName(user.getName());
		}catch(Exception e){}
		if (o1 == null && o2 ==null){
			userDao.persist(user);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Return a user for a given user name and password
	 * @param user
	 * @return
	 */
	public User getUserForNameAndPas(User user){	
		
		try{
			user = userDao.getUserForNameAndPas(user.getName(), user.getPassword());
			return user;
		}catch(Exception e){
			return null;
		}
	}
	
	
	/**
	 * Returns all the favorite recipes for user
	 * @param user
	 * @return
	 */
	public ArrayList<Recipe> getFavorites(User user){
			user = userDao.getUserForMail(user.getMail());
			HashMap<Integer,Recipe> recipeMap = user.getFavoriteRecipes();
			ArrayList<Recipe> recipes =  new ArrayList<Recipe>(recipeMap.values());
			return recipes;	
	}
	
	/**
	 * Returns all the cart recipes for user
	 * @param user
	 * @return
	 */
	public ArrayList<Recipe> getCart(User user){
			user = userDao.getUserForMail(user.getMail());
			HashMap<Integer,Recipe> recipeMap = user.getCartRecipes();
			ArrayList<Recipe> recipes =  new ArrayList<Recipe>(recipeMap.values());
			return recipes;	
	}
	
	/**
	 * updates a user
	 * @param user
	 * @return
	 */
	public User updateUser(User user){
			user = userDao.updateUser(user);
			return user;	
	}
	
	/**
	 * returns a user sfor an email
	 * @param user
	 * @return
	 */
	public User getUserForMail(User user){
			
			return userDao.getUserForMail(user.getMail());	
	}
	
	
	
	
}

