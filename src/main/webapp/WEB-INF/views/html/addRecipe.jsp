<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Adios Mam&aacute;</title>
    <meta name="description" content="Las recetas de mam&acute; hechas por ti">
    <meta name="viewport" content="width=device-width">

	<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-v1.8.0.js"></script>
	<script src="${pageContext.request.contextPath}/static/js/ajaxupload.js" type="text/javascript"></script>


    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/normalize.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css">
    <script src="${pageContext.request.contextPath}/static/js/vendor/modernizr-2.6.2.min.js"></script>
		<script type="text/javascript">
			var baseUrl = "${pageContext.request.contextPath}";
		</script>
    <script src="${pageContext.request.contextPath}/static/js/addRecipie.js"></script>

</head>

<body>
  <div class="ads-top"><!-- Empty Div --></div>
  <div class="main">
    <!--header-->
    <div id="header" class="clearfix">
      <!-- Dejo espacio para el logo clickable, pensando en un futuro!!! -->
      <div class="logo">
        <a href="${pageContext.request.contextPath}/welcome.html" title="Adios Mam&aacute;">Adios Mam&aacute;</a>
      </div>
      
			<!-- Menu -->
			<jsp:include page="./components/menu.jsp" />
			
      </div>
    </div>
    <!--header end-->

    <!--content -->
    <div class="content">
      <div class="content-bg clearfix">
        <div class="side-bar-left">
          <div class="ind">
          

			<!-- Search form -->
            <jsp:include page="./components/searchForm.jsp" />


			<!-- filters -->
			<jsp:include page="./components/filters.jsp" />

			<!-- ing Cloud -->
			<jsp:include page="./components/ingCloud.jsp" />

			<!-- tag Cloud -->
			<jsp:include page="./components/tagCloud.jsp" />

          </div>
        </div>
        
        
        <div class="column-center">
          <div class="coffee-mark"><!--Empty Div --></div>
          <div class="wrapper">
            <h4 class="box">A�adir nueva receta</h4>
            <div id="indent-center" class="indent-center">
            
<!--       ---------------------------------------------------------------------------------------------------------       -->
<!--       ---------------------------------------------------------------------------------------------------------       -->
<!-- content AREA -->
<!--       ---------------------------------------------------------------------------------------------------------       -->
<!--       ---------------------------------------------------------------------------------------------------------       -->
            

							<div class="post">
								<div class="title">
<!-- 									<div class="date">29Apr</div> -->
								</div>
								<div class="text-box">
									<div class="ind">
										<div>
											<p>
											<label>nombre</label> <input type="text" name="name"
													id="name" /> <br />
												<label>descripcion</label>
												<textarea name="desc" id="desc" style="width:500px;  height:200px;"></textarea> 															
												<br /><br />
												imagen <input id="imgRecipe" type='text'
												value='' /><br /> <br /> video link 
												<input id="externalMedia" type='text' value='' /><br />
													    
													    <br />
													<div id="addIngredientDiv">
														<label>ingrediente (nombre/cantidad/unidad)</label> 
														<input
															type="text" name="ingredient" id="ingredient0" /> 
															<input
															type="text" name="quantity" id="quantity0" /> 													
															<select id="unit0">
															   <option value="gr" selected="selected">gramos</option>
															   <option value="cl">centilitros</option>
															</select>
													</div>
													<img	id="addIngredient"
															style="max-width: 40px; max-height: 40px;" alt="addIngredient"
															src="${pageContext.request.contextPath}/static/img/addBtn.png"><br>

												<br /> 
													<div id="addTagDiv">
														<label>tags</label> 
														<input
															type="text" name="tag" id="tag0" /> 															
													</div>
													<img	id="addTag"
															style="max-width: 40px; max-height: 40px;" alt="addTag"
															src="${pageContext.request.contextPath}/static/img/addBtn.png"><br>

												<br /> 

													<div id="addStepDiv">
														<label>paso</label> 
														<textarea name="step" id="step0" ></textarea> 															
													</div>
													<img	id="addStep"
															style="max-width: 40px; max-height: 40px;" alt="addStep"
															src="${pageContext.request.contextPath}/static/img/addBtn.png"><br>

												<br /> 

													<img	id="okBtn"
															style="max-width: 20px; max-height: 20px;" alt="addStep"
															src="${pageContext.request.contextPath}/static/img/okBtn.jpg"><br>

											</p>
										</div>


										<br>
									</div>
								</div>
							</div>
<!--       ---------------------------------------------------------------------------------------------------------       -->
<!--       ---------------------------------------------------------------------------------------------------------       -->
<!--       ---------------------------------------------------------------------------------------------------------       -->
<!--       ---------------------------------------------------------------------------------------------------------       -->						
						
						
						
            </div>
           </div>
        </div>
      </div>
    </div>
    <!--content end-->


    <!-- footer -->
	<jsp:include page="./components/footer.jsp" />


</body>
</html>







							












