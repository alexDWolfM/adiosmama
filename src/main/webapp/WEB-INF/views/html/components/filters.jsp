<!-- -------------------------------------------------------------------- -->
<!-- filtros ingredientes -->
<!-- -------------------------------------------------------------------- -->
<div class="widget filter" id="filter">
	<h2>
		<span>Filtros</span>
	</h2>
	<div class="inside-widget">
		<ul class="list">
		</ul>
		<form id="search-category" class="search-category"
			onSubmit="return false;">
			<label for="search-input-i"></label> <input class="searching"
				name="s" id="search-input-i" type="text" size="16"
				placeholder="Buscar recetas ..." /> <input type="hidden"
				name="type" value="i" />
		</form>

		<br>
			<div id="searchBtn" class="wrapper" >
			<h4 class="box">Buscar</h4>
			</div>
			<br>

	</div>

	<!-- -------------------------------------------------------------------- -->
	<!-- filtros activos -->
	<!-- -------------------------------------------------------------------- -->
	<h2>
		<span></span>
	</h2>
	<div class="inside-widget">
		<form method="get" id="filter-form" class="filter-form"
			action="amazing-search.html">
			<ul id="filter-list" class="list">


			</ul>
		</form>
	</div>
</div>
