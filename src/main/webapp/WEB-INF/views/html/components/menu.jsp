      <div class="menu">
        <ul class="menu-item-list">
          <li class="menu-item current_page_item">
            <a href="${pageContext.request.contextPath}/welcome.html">Novedades</a>
          </li>
          <li class="menu-item" >
            <a href="${pageContext.request.contextPath}/user/recetas/favoritas.html">Mis Favoritos</a>
          </li>
          <li class="menu-item">
            <a href="${pageContext.request.contextPath}/user/receta/compra/lista.html">La Compra</a>
          </li>
          <li class="menu-item">
            <a href="${pageContext.request.contextPath}/user/receta/lasRecetas.html">Las Recetas</a>
          </li>
          <li class="menu-item">
            <a href="${pageContext.request.contextPath}/user/welcome.html">Usuario</a>
          </li>
        </ul>
