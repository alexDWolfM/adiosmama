
<div class="widget">

	<!-- -------------------------------------------------------------------- -->
	<!-- filtros por Tag comida desayuno cena -->
	<!-- -------------------------------------------------------------------- -->
	<div class="categories" id="tags">
		<h2>
			<span>Tags</span>
		</h2>

		<div id="tag_cloud">
			<!--
              <h2>Tags</h2>
 -->
			<div>

				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[9]}.html"
					class="tag-link-13" title="1 topic" style="font-size: 8pt;">${searchTags[9]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[0]}.html"
					class="tag-link-10" title="2 topics" style="font-size: 22pt;">${searchTags[0]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[8]}.html"
					class="tag-link-11" title="1 topic" style="font-size: 10pt;">${searchTags[8]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[4]}.html"
					class="tag-link-12" title="1 topic" style="font-size: 14pt;">${searchTags[4]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[7]}.html"
					class="tag-link-13" title="1 topic" style="font-size: 8pt;">${searchTags[7]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[2]}.html"
					class="tag-link-14" title="2 topics" style="font-size: 18pt;">${searchTags[2]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[6]}.html"
					class="tag-link-15" title="1 topic" style="font-size: 8pt;">${searchTags[6]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[3]}.html"
					class="tag-link-16" title="1 topic" style="font-size: 16pt;">${searchTags[3]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[5]}.html"
					class="tag-link-17" title="1 topic" style="font-size: 12pt;">${searchTags[5]}</a>
				<a
					href="${pageContext.request.contextPath}/busqueda/tag/${searchTags[1]}.html"
					class="tag-link-18" title="2 topics" style="font-size: 20pt;">${searchTags[1]}</a>
			</div>

			<div class="inside-widget">
				<!-- 			<ul class="list"> -->
				<!-- 				<li class="item cat-item-1"><a href="poraquivoyyo.html" -->
				<!-- 					title="Zanahorias buenas para la vista">Desayuno</a></li> -->
				<!-- 			</ul> -->
				<form method="get" id="search-tags" class="search-category"
					onsubmit="return false;">
					<label for="search-input-t"></label> <input class="searching"
						name="s" id="search-input-tag" type="text" size="16"
						placeholder="Buscar tags ..."> <input type="hidden"
						name="type" value="t" />
				</form>
			</div>
		</div>

	</div>
</div>