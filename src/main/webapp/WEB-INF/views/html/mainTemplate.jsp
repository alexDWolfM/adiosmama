<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Adios Mam&aacute;</title>
<meta name="description"
	content="Las recetas de mam&acute; hechas por ti">
<meta name="viewport" content="width=device-width">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/normalize.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/css/main.css">
<script
	src="${pageContext.request.contextPath}/static/js/vendor/modernizr-2.6.2.min.js"></script>

</head>
<body>
	<div class="ads-top">
		<!-- Empty Div -->
	</div>
	<div class="main">
		<!--header-->
		<div id="header" class="clearfix">
			<!-- Dejo espacio para el logo clickable, pensando en un futuro!!! -->
			<div class="logo">
				<a href="${pageContext.request.contextPath}/welcome.html"
					title="Adios Mam&aacute;">Adios Mam&aacute;</a>
			</div>

			<!-- Menu -->
			<jsp:include page="./components/menu.jsp" />

		</div>
	</div>
	<!--header end-->

	<!--content -->
	<div class="content">
		<div class="content-bg clearfix">
			<div class="side-bar-left">
				<div class="ind">


					<!-- Search form -->
					<jsp:include page="./components/searchForm.jsp" />


					<!-- filters -->
					<jsp:include page="./components/filters.jsp" />

					<!-- ing Cloud -->
					<jsp:include page="./components/ingCloud.jsp" />

					<!-- tag Cloud -->
					<jsp:include page="./components/tagCloud.jsp" />

				</div>
			</div>


			<div class="column-center">
				<div class="coffee-mark">
					<!--Empty Div -->
				</div>
				<div class="wrapper">
					<h4 class="box">Blog</h4>
					<div id="indent-center" class="indent-center">

						<jsp:include page="./components/recipeListDisplay.jsp" />

					</div>
				</div>


				<!--pagination start-->
				<div id="listitems-pagination" style="float:right;">
					<a id="listitems-previous" href="${pageContext.request.contextPath}/page/${prevPage}/welcome.html" class="disabled">&laquo;Anterior</a> 
					<a id="listitems-next" href="${pageContext.request.contextPath}/page/${nextPage}/welcome.html">Siguiente &raquo;</a>
				</div>
				<!--pagination end-->


			</div>
		</div>
	</div>
	<!--content end-->

	<!-- footer -->
	<jsp:include page="./components/footer.jsp" />


</body>
</html>
