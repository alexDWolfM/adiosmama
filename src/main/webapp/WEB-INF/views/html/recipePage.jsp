<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Adios Mam&aacute;</title>
  <meta name="description" content="Las recetas de mam&acute; hechas por ti">
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/normalize.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css">
  <script src="${pageContext.request.contextPath}/static/js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
<div class="ads-top"><!-- Empty Div --></div>
<div class="main">
<!--header-->
<div id="header" class="clearfix">
  <!-- Dejo espacio para el logo clickable, pensando en un futuro!!! -->
  <div class="logo">
    <a href="${pageContext.request.contextPath}/welcome.html" title="Adios Mam&aacute;">Adios Mam&aacute;</a>
  </div>
  <div class="menu">
    <ul class="menu-item-list">
      <li class="menu-item current_page_item">
        <a href="${pageContext.request.contextPath}/welcome.html">Novedades</a>
      </li>
      <li class="menu-item">
        <a href="${pageContext.request.contextPath}/user/welcome.html">Mis Favoritos</a>
      </li>
      <li class="menu-item">
        <a href="${pageContext.request.contextPath}/user/welcome.html">La Compra</a>
      </li>
      <li class="menu-item">
        <a href="#">Las Recetas</a>
      </li>
      <li class="menu-item">
        <a href="${pageContext.request.contextPath}/user/welcome.html">Usuario</a>
      </li>
    </ul>
  </div>
</div>
<!--header end-->

<!--content -->
<div class="content">
  <div class="content-bg clearfix">
    <div class="side-bar-left">
      <div class="ind">

			<!-- Search form -->
            <jsp:include page="./components/searchForm.jsp" />


			<!-- filters -->
			<jsp:include page="./components/filters.jsp" />

			<!-- ing Cloud -->
			<jsp:include page="./components/ingCloud.jsp" />

			<!-- tag Cloud -->
			<jsp:include page="./components/tagCloud.jsp" />

      </div>
    </div>
    <div class="column-center">
      <div class="coffee-mark"><!--Empty Div --></div>
      <div class="wrapper">


        <div id="indent-center" class="indent-center">
          <div class="article">
            <div class="post">
              <div class="content-full">
                <div class="title">
                  <h2>
                    <a href="#" rel="bookmark" title="">${recipe.name}</a>
                  </h2>
                </div>
                <div class="body">
                  <div class="cell content-media">
                    <img alt="" src="${recipe.imageName}"><br>
                  </div>
                  <div class="description">
                    ${recipe.description}
                  </div>
                  <div class="media">
                    ${recipe.externalMedia}
                  </div>
                  
                  <div class="tags">
                  <span>Ingrdientes:</span>
                    <c:forEach items="${ingredients}" var="ing" varStatus="i">
                      <div class="step"> <span class="tag">${ing.name}</span> ${ing.quantity} ${ing.unit}</div>

                    </c:forEach>
                  </div>

                  
                  <div class="steps">
                    <c:forEach items="${recipe.instructions}" var="step" varStatus="i">
                      <h3>Paso:<c:out value="${i.index+1}"/></h3>
                      <div class="step">${step}</div>
                    </c:forEach>
                  </div>
                  <div class="tags">
                    <span>Tags de la receta:</span>
                    <c:forEach items="${tags}" var="tag" varStatus="i">
                      <span class="tag">
                        ${tag.name}
                      </span>
                    </c:forEach>
                  </div>
                </div>
                <!-- Place this tag where you want the +1 button to render. -->
                <div class="g-plusone" data-size="medium" data-annotation="inline" data-width="300"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--content end-->

	<!-- footer -->
	<jsp:include page="./components/footer.jsp" />
	
	<!-- footer -->
	<jsp:include page="./components/footer.jsp" />
</body>
</html>
